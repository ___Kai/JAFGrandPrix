﻿using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Actors/Patrol Set")]
public class Patrol : MonoBehaviour
{
    [HideInInspector]
    public List<WayPoint> waypoints = new List<WayPoint>();


    [ContextMenu("Add Linked Waypoint")]
    public void AddWaypointToPath()
    {
        GameObject newWaypoint = new GameObject();
        newWaypoint.transform.parent = transform;
        if (waypoints.Count > 1)
        {
            newWaypoint.transform.localPosition = waypoints[waypoints.Count - 1].transform.localPosition;
        }
        else
        {
            newWaypoint.transform.localPosition = transform.localPosition;
        }
        newWaypoint.AddComponent<WayPoint>();
        newWaypoint.name = "waypoint";
        newWaypoint.GetComponent<WayPoint>().patrol = this;
        waypoints.Add(newWaypoint.GetComponent<WayPoint>());
        if (waypoints.Count > 1 && waypoints.Count < 3)
        {
            waypoints[0].nextWaypoint = newWaypoint.GetComponent<WayPoint>();
        }
        else if (waypoints.Count > 1)
        {
            waypoints[waypoints.Count - 2].nextWaypoint = newWaypoint.GetComponent<WayPoint>();
        }


    }

    [ContextMenu("Complete Path")]
    public void LinkWaypointToStart()
    {
        if (waypoints.Count > 1)
        {
            waypoints[waypoints.Count - 1].nextWaypoint = waypoints[0];
        }
    }

    public void RemoveWayPoint(WayPoint waypointToRemove)
    {
        waypoints.Remove(waypointToRemove);
        ReLinkWayPoints();
    }

    private void ReLinkWayPoints()
    {
        if (waypoints.Count > 1)
        {
            for (int i = 0; i < waypoints.Count; i++)
            {
                if (i < waypoints.Count - 1)
                {
                    waypoints[i].nextWaypoint = waypoints[i + 1];
                }
            }
        }
        else
        {
            Debug.Log("Not Enough waypoints created, there must be atleast 2 or more");
        }

    }

    public void UnlinkPath()
    {
        if (waypoints.Count > 1)
        {
            waypoints[waypoints.Count - 1].nextWaypoint = null;
        }
    }

    public void ClearAllWayPoints()
    {
        for (int i = 0; i < waypoints.Count; i++)
        {
            if (waypoints[i] != null)
            {
                DestroyImmediate(waypoints[i].gameObject);
            }
        }
        waypoints.Clear();
    }
}


