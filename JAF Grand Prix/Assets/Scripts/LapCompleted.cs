using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LapCompleted : MonoBehaviour
{
    public GameObject lapCompletedTrigg;
    public GameObject halfLapTrigg;
    public GameObject minuteDisplay;
    public GameObject secondDisplay;
    public GameObject millisecondDisplay;
    public GameObject lapTimeBox;

    void OnTriggerEnter()
    {
        if(LapTimeManager.secondsCount <= 9)
        {
            secondDisplay.GetComponent<Text>().text = "0" + LapTimeManager.secondsCount + ".";
        }
        else
        {
            secondDisplay.GetComponent<Text>().text = "" + LapTimeManager.secondsCount + ".";
        }
        if (LapTimeManager.minuteCount <= 9)
        {
            minuteDisplay.GetComponent<Text>().text = "0" + LapTimeManager.minuteCount + ".";
        }
        else
        {
            minuteDisplay.GetComponent<Text>().text = "" + LapTimeManager.minuteCount + ".";
        }
        millisecondDisplay.GetComponent<Text>().text = "" + LapTimeManager.millisecondCount;
        LapTimeManager.minuteCount = 0;
        LapTimeManager.secondsCount = 0;
        LapTimeManager.millisecondCount = 0;
        halfLapTrigg.SetActive(true);
        lapCompletedTrigg.SetActive(false);
    }
}
